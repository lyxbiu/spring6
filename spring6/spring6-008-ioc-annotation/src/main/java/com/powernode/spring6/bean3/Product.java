package com.powernode.spring6.bean3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Product {
    /*@Value("隔壁老王")
    private String name;
    @Value("20")
    private int age;*/

    private String name;
    private int age;

    // @Value也可以用在构造方法的形参前
    public Product(@Value("隔离老王") String name, @Value("20") int age) {
        this.name = name;
        this.age = age;
    }

    // @Value注解也可以用在setter方法上
    /*@Value("20")
    public void setAge(int age) {
        this.age = age;
    }

    @Value("隔壁老王")
    public void setName(String name) {
        this.name = name;
    }*/

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

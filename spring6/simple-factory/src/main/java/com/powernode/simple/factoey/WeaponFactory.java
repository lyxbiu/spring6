package com.powernode.simple.factoey;

// 工厂角色
public class WeaponFactory {
    /**
     * 静态方法，不需要创建对象
     *传TANK获取坦克 传DAGGER获取匕首 传FIGHTER获取战斗机
     * 简单工厂模式中有一个静态方法，所以被称为：静态工厂方法模式
     * @param weaponName
     * @return
     */
    public static Weapon get(String weaponName){
        if("TANK".equals(weaponName)){
            return new Tank();
        }else if("DAGGER".equals(weaponName)){
            return new Dagger();
        }else if("FIGHTER".equals(weaponName)){
            return new Fighter();
        }else{
            throw  new RuntimeException("不支持该武器的生产");
        }
    }
}

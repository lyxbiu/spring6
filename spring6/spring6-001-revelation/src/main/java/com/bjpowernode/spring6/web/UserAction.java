package com.bjpowernode.spring6.web;

import com.bjpowernode.spring6.service.UserService;
import com.bjpowernode.spring6.service.impl.UserServiceImpl;

public class UserAction {
    private UserService userService=new UserServiceImpl();

    public void deleteRequest(){
        userService.deleteUser();
    }
}

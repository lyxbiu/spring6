package com.powernode.proxy.service;

// 订单业务接口（代理对象和目标对象的公共接口）
public interface OrderService {
    void generate();

    void modify();

    void detail();
}

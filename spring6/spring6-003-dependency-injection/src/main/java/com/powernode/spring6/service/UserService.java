package com.powernode.spring6.service;

import com.powernode.spring6.dao.UserDao;

public class UserService {
    private UserDao userDao;

    // set注入的话，必须提供一个set方法
    // spring容器会调用set方法，来给userDao属性赋值
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void insertUser(){
        userDao.insert();
    }
}

package com.powernode.spring6.bean;

// 简单工厂模式中工厂类角色
public class StarFactory {
    // 工厂类中有一个静态方法
    // 简单工厂模式又叫做：静态工厂方法模式
    public static Star get(){
        return new Star();
    }
}

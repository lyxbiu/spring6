package com.powernode.spring6.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FirstSpringTest {
    @Test
    public void testFirstSpring(){
        // 第一步：获取spring容器对象
        // ApplicationContext 翻译为：应用上下文。其实就是spring容器
        // ApplicationContext 接口下有很多实现类，其中有一个实现类叫做：ClassPathXmlApplicationContext
        // ClassPathXmlApplicationContext 专门从类根路径下加载spring配置文件的一个spring上下文对象
        // 如果不是从类的根路径下加载资源，可以使用FileSystemXmlApplicationContext这个实现类
        // 这行代码只要执行，就相当于启动了spring容器，解析spring.xml文件，并且实例化所有的bean对象，放到spring容器中
        // ApplicationContext接口的超级父接口是：BeanFactory（翻译为bean工厂，就是能生产bean对象的一个工厂对象）
        // BeanFactory是IoC容器的顶级接口
        // spring底层的IoC是怎么实现的？XML解析 + 工厂模式 + 反射机制
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring6.xml","beans.xml");

        // 第二步：根据bean的id从Spring容器中获取这个对象
        Object userBean = applicationContext.getBean("userBean");
        System.out.println(userBean);

        Object vipBean = applicationContext.getBean("vipBean");
        System.out.println(vipBean);

        // 如果bean的id不存在，不会返回null，而是会出现异常
        // Date nowTime = (Date)applicationContext.getBean("nowTime");
        // 不想强制类型转换，可以使用以下代码，通过第二个参数指定返回的bean的类型
        Date nowTime = applicationContext.getBean("nowTime", Date.class);
        // System.out.println(nowTime);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
        String strNowTime = sdf.format(nowTime);
        System.out.println(strNowTime);
    }

    @Test
    public void testLog4j2(){
        new ClassPathXmlApplicationContext("spring6.xml");

        // 你自己怎么使用log4j2记录日志信息呢?
        // 第一步：创建日志记录器对象
        // 获取FirstSpringTest类的日志记录器对象，也就是说只要是FirstSpringTest类中的代码执行记录日志的话，就输出相关的日志信息
        Logger logger= LoggerFactory.getLogger(FirstSpringTest.class);

        // 第二步：记录日志，根据不同的级别来输出日志
        logger.info("我是一条消息");
        logger.debug("我是一条调试信息");
        logger.error("我是一条错误信息");
    }
}

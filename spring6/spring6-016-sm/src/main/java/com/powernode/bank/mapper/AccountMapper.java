package com.powernode.bank.mapper;

import com.powernode.bank.pojo.Account;

import javax.swing.*;
import java.util.List;

public interface AccountMapper { //该接口的实现类不需要写，是mybatis通过动态代理机制生成的实现类
    /**
     * 新增账户
     * @param account
     * @return
     */
    int insert(Account account);

    /**
     * 根据账号删除账户
     * @param actno
     * @return
     */
    int deleteByActno(String actno);

    /**
     * 更新账户
     * @param account
     * @return
     */
    int update(Account account);

    /**
     * 根据账号查询账户
     * @param actno
     * @return
     */
    Account selectByActno(String actno);

    /**
     * 查询所有账户
     * @return
     */
    List<Account> selectAll();
}

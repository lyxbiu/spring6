package org.powernode.spring6.service;

import org.powernode.spring6.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("orderWiredService")
public class OrderWiredService {
    // @Autowired注解使用的时候，不需要指定任何属性，直接使用这个注解即可
    // 这个注解的作用是根据byType自动装配
    // 自动注入必须只能有一个类型
    // @Autowired
    // private OrderDao orderDao;

    // 如果想解决以上问题，只能根据名字进行装配
    // @Autowired 和 @Qualifier联合使用，可以根据名字进行装配
    /*@Autowired
    @Qualifier("orderDaoImplForOracle")
    private OrderDao orderDao;*/

    private OrderDao orderDao;

    // @Autowired也可以出现在setter方法上
    /*@Autowired
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }*/

    // @Autowired也可以出现在构造方法上
    /*@Autowired
    public OrderWiredService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }*/

    // @Autowired也可以出现在构造方法的参数前
    /*public OrderWiredService(@Autowired OrderDao orderDao) {
        this.orderDao = orderDao;
    }*/

    public OrderWiredService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    // 如果一个类当中只有一个，并且构造方法的参数和属性能够对应上，@Autowired可以省略
    /*public OrderWiredService() {
    }*/

    public void generate(){
        orderDao.insert();
    }
}

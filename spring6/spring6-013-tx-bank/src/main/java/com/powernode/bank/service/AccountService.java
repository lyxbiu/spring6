package com.powernode.bank.service;

import com.powernode.bank.pojo.Account;

public interface AccountService {
    /**
     * 转账业务
     * @param from 从这个账户转出
     * @param to 从这个账户转入
     * @param money 转账金额
     */
    void transfer(String from,String to,Double money);

    void save(Account act);
}

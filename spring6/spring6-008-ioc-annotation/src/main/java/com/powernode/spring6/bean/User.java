package com.powernode.spring6.bean;

import org.springframework.stereotype.Component;

@Component("userBean")
public class User {

}

/**
 * 以上这个注解@Component就相当于以下配置
 * <bean id="userBean" class="com.powernode.spring6.bean.Product"/>
 */

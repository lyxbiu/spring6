package com.powernode.proxy.client;

import com.powernode.proxy.service.TimeMethodInterceptor;
import com.powernode.proxy.service.UserService;
import net.sf.cglib.proxy.Enhancer;

public class Client {
    public static void main(String[] args) {
        // 创建字节码增强对象
        // 这个对象是CGLIB库当中的核心对象，就是靠它来生成代理类
        Enhancer enhancer=new Enhancer();

        // 告诉CGLIB父类是谁，就是告诉CGLIB目标对象是谁
        enhancer.setSuperclass(UserService.class);

        // 设置回调（等同于JDK动态代理当中的调研处理器，InvocationHandler）
        // 在CGLIB当中不是InvocationHandler接口，是方法拦截器接口：MethodInterceptor
        enhancer.setCallback(new TimeMethodInterceptor());

        // 创建代理对象
        // 这一步会做两件事
        // 第一件事：在内存中生成UserService类的子类，其实就是代理类的字节码
        // 第二件事：创建代理对象
        UserService userServiceProxy = (UserService) enhancer.create();

        // 调用代理对象的代理方法
        boolean success = userServiceProxy.login("admin", "123");
        System.out.println(success?"登录成功":"登陆失败");

        userServiceProxy.loginOut();

    }
}

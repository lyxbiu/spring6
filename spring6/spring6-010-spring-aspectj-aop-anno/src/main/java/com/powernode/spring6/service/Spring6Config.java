package com.powernode.spring6.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

// 全注解式开发
@Configuration // 代替Spring.xml文件
@ComponentScan("com.powernode.spring6.service") // 组件扫描
@EnableAspectJAutoProxy(proxyTargetClass = true) // 启用aspectj的CGLIB自动代理机制
public class Spring6Config {
}

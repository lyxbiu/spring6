package com.powernode.client;

import com.powernode.annotation.Component;

public class ReflectAnnotationTest1 {
    public static void main(String[] args) throws Exception {
        // 通过反射机制获取注解
        // 获取类
        Class<?> clazz = Class.forName("com.powernode.bean.User");
        // 判断类上是否有这个注解
        if (clazz.isAnnotationPresent(Component.class)) {
            // 获取类上注解
            Component annotation = clazz.getAnnotation(Component.class);
            // 访问注解属性
            System.out.println(annotation.value());
        }
    }
}

package cn.powernode.spring6;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 编写一个类，代替Spring框架的配置文件
 */
@Configuration
@ComponentScan({"cn.powernode.spring6.dao","cn.powernode.spring6.service"})
public class Spring6Config {
}

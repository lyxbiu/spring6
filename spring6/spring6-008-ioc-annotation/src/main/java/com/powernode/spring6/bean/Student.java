package com.powernode.spring6.bean;

import org.springframework.stereotype.Repository;

// 如果属性名是value，属性名可以省略
// 如果不给value赋值，类名首字母小写就是Bean的名字
@Repository("studentBean")
public class Student {
}

/**
 * 以上这个注解@Repository就相当于以下配置
 * <bean id="studentBean" class="com.powernode.spring6.bean.Student"/>
 */
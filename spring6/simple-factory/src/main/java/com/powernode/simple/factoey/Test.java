package com.powernode.simple.factoey;

// 客户端程序
public class Test {
    public static void main(String[] args) {
        // 需要坦克
        // 对于客户端来说，坦克的生产细节，我不关心，我只需要向工厂索要即可
        // 简单工厂模式达到了什么呢？职责分离。客户端不需要关心产品的生产细节
        // 客户端只负责消费，工厂负责生产。一个负责生产，一个负责消费。生产者和消费者分离了。这就是简单工厂模式的作用
        WeaponFactory.get("TANK").attack();
        WeaponFactory.get("DAGGER").attack();
        WeaponFactory.get("FIGHTER").attack();
    }
}

package cn.powernode.spring6.dao.impl;

import cn.powernode.spring6.dao.StudentDao;
import org.springframework.stereotype.Repository;

@Repository("studentDaoImpl")
public class StudentDaoImpl implements StudentDao {
    @Override
    public void deleteById() {
        System.out.println("MySQL数据库正在删除学生!!!");
    }
}

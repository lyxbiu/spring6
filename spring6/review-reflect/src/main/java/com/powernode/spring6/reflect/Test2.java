package com.powernode.spring6.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class Test2 {
    public static void main(String[] args) throws Exception {
        // 使用反射机制调用方法
        // 获取类
        Class<?> clazz = Class.forName("com.powernode.spring6.reflect.SomeService");

        // 获取方法
        Method doSomeMethod = clazz.getDeclaredMethod("doSome", String.class, int.class);

        // 调用方法
        // Constructor<?> con = clazz.getDeclaredConstructor();
        // Object obj = con.newInstance();
        Object obj = clazz.newInstance();
        Object reValue = doSomeMethod.invoke(obj, "李四", 20);
        System.out.println(reValue);
    }
}

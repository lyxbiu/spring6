package com.powernode.spring6.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test3 {
    public static void main(String[] args) throws Exception{
        /**
         * 需求：
         *      假设你现在已知以下信息：
         *          1.在这个类中，类名叫：com.powernode.spring6.reflect.User
         *          2.这个类符合javabean规范，属性私有化，对外提供公开的setter和getter方法
         *          3.你还知道这个类当中有一个属性，属性名叫：age
         *          4.并且你还知道age属性的类型是int类型
         *      请使用反射机制调用set方法，给User对象的age属性赋值
         */
        String className="com.powernode.spring6.reflect.User";
        String propertyName="age";

        // 通过反射机制调用setAge(int)方法
        // 获取类
        Class<?> clazz = Class.forName(className);
        // 获取方法名
        String setMethodName="set"+propertyName.toUpperCase().charAt(0)+propertyName.substring(1);
        // 根据属性名获取属性类型
        Field field = clazz.getDeclaredField(propertyName);
        // 获取方法
        Method setMethod = clazz.getDeclaredMethod(setMethodName, field.getType());
        // 调用方法
        Object obj = clazz.newInstance();
        setMethod.invoke(obj,20);

        System.out.println(obj);
    }
}

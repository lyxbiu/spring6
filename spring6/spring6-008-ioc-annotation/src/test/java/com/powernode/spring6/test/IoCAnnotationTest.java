package com.powernode.spring6.test;

import cn.powernode.spring6.Spring6Config;
import cn.powernode.spring6.service.StudentService;
import com.powernode.spring6.bean.Order;
import com.powernode.spring6.bean.Student;
import com.powernode.spring6.bean.User;
import com.powernode.spring6.bean.Vip;
import com.powernode.spring6.bean3.MyDataSource;
import com.powernode.spring6.bean3.Product;
import com.powernode.spring6.dao.OrderDao;
import org.junit.Test;
import org.powernode.spring6.service.OrderWiredService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IoCAnnotationTest {
    @Test
    public void testBeanComponent(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        User userBean = applicationContext.getBean("userBean", User.class);
        System.out.println(userBean);

        Vip vipBean = applicationContext.getBean("vipBean", Vip.class);
        System.out.println(vipBean);

        Order orderBean = applicationContext.getBean("order", Order.class);
        System.out.println(orderBean);

        Student studentBean = applicationContext.getBean("studentBean", Student.class);
        System.out.println(studentBean);

        OrderDao orderDao = applicationContext.getBean("orderDao", OrderDao.class);
        System.out.println(orderBean);
    }

    @Test
    public void testChoose(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring-choose.xml");
    }

    @Test
    public void testDIByAnnotation(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring-di-annotation.xml");
        MyDataSource myDataSource = applicationContext.getBean("myDataSource", MyDataSource.class);
        System.out.println(myDataSource);

        Product product = applicationContext.getBean("product", Product.class);
        System.out.println(product);
    }

    @Test
    public void testAutoWired(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring-autowired.xml");
        OrderWiredService orderWiredService = applicationContext.getBean("orderWiredService", OrderWiredService.class);
        orderWiredService.generate();
    }

    @Test
    public void testResource(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring-resource.xml");
        StudentService studentService = applicationContext.getBean("studentService", StudentService.class);
        studentService.deleteStudent();
    }

    @Test
    public void testNoXml(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Spring6Config.class);
        StudentService studentService = context.getBean("studentService", StudentService.class);
        studentService.deleteStudent();
    }

}

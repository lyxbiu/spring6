package com.powernode.spring6.biz;

import org.springframework.stereotype.Service;

@Service
public class VipService { // 目标对象
    public void saveVip(){
        System.out.println("新增会员...");
    }

    public void deleteVip(){
        System.out.println("删除会员...");
    }

    public void modifyVip(){
        System.out.println("修改会员...");
    }

    public void getVip(){
        System.out.println("获取会员...");
    }
}

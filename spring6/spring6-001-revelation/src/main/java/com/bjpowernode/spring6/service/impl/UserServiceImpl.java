package com.bjpowernode.spring6.service.impl;

import com.bjpowernode.spring6.dao.UserDao;
import com.bjpowernode.spring6.dao.impl.UserDaoImpl;
import com.bjpowernode.spring6.service.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao=new UserDaoImpl();

    @Override
    public void deleteUser() {
        userDao.deleteById();
    }
}

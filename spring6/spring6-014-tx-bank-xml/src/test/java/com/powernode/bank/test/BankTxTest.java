package com.powernode.bank.test;

import com.powernode.bank.dao.AccountDao;
import com.powernode.bank.service.AccountService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BankTxTest {
    @Test
    public void testNoAnnotation(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        try {
            accountService.transfer("act-001","act-002",10000.0);
            System.out.println("转账成功");
        } catch (Exception e) {
            System.out.println("转账异常");
            e.printStackTrace();
        }
    }
}

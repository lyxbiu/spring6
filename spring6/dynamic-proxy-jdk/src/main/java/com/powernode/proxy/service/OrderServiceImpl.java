package com.powernode.proxy.service;

// 目标对象
public class OrderServiceImpl implements OrderService {

    @Override
    public String getName() {
        System.out.println("getName方法执行了");
        return "张三";
    }

    @Override
    public void generate() {  // 目标方法
        // 模拟生成订单的耗时
        try {
            Thread.sleep(1234);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("订单已生成！！！");}

    @Override
    public void modify() {
        // 模拟修改订单的耗时
        try {
            Thread.sleep(1234);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("订单已修改！！！");
    }

    @Override
    public void detail() {
        // 模拟查询订单的耗时
        try {
            Thread.sleep(1234);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("查看订单详情！！！");
    }
}

package cn.powernode.spring6.service;

import cn.powernode.spring6.dao.StudentDao;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service("studentService")
public class StudentService {
    // @Resource (name="studentDaoImpl")
    @Resource
    private StudentDao studentDao;

    // @Resource也可以出现在setter方法上
    /*@Resource (name="studentDaoImpl")
    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }*/

    public void deleteStudent(){
        studentDao.deleteById();
    }
}

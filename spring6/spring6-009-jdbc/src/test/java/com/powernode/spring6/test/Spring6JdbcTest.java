package com.powernode.spring6.test;

import com.powernode.spring6.bean.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Spring6JdbcTest {
    @Test
    public void testJdbc(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        System.out.println(jdbcTemplate);
    }

    @Test
    public void testInset(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        // insert语句
        String sql="insert into t_user(real_name,age) values(?,?)";
        // 注意：在JdbcTemplate当中，只要是insert、delete、update语句都是调用update方法
        int count = jdbcTemplate.update(sql, "王五", 20);
        System.out.println(count);
    }

    @Test
    public void  testUpdate(){
        // 根据id来修改一条记录
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="update t_user set real_name=?,age=? where id=?";
        int count = jdbcTemplate.update(sql, "张三丰", 55, 1);
        System.out.println(count);
    }

    @Test
    public void  testDelete(){
        // 根据id来删除一条记录
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="delete from t_user where id=?";
        int count = jdbcTemplate.update(sql, 1);
        System.out.println(count);
    }

    @Test
    public void  testQueryOne(){
        // 根据id来查询一条记录
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="select id,real_name,age from t_user where id=?";
        User user = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(User.class),2);
        System.out.println(user);
    }

    @Test
    public void  testQueryAll(){
        // 查询所有记录
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="select id,real_name,age from t_user";
        List<User> users = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(User.class));
        System.out.println(users);
    }

    @Test
    public void testQueryOneValue(){
        // 查询某个值
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="select count(*) from t_user";
        Integer total = jdbcTemplate.queryForObject(sql, int.class);
        System.out.println(total);
    }

    @Test
    public void testBatchInset(){
        // 插入多条语句
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="insert into t_user(real_name,age) values(?,?)";

        // 准备数据
        Object[] obj1s={"小花1",30};
        Object[] obj2s={"小花2",31};
        Object[] obj3s={"小花3",32};
        Object[] obj4s={"小花4",33};

        // 添加到List集合
        List<Object[]> list=new ArrayList<>();
        list.add(obj1s);
        list.add(obj2s);
        list.add(obj3s);
        list.add(obj4s);

        // 执行sql
        int[] count = jdbcTemplate.batchUpdate(sql, list);
        System.out.println(Arrays.toString(count));
    }

    @Test
    public void testBatchUpdate(){
        // 修改多条语句
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="update t_user set real_name=?,age=? where id=?";

        // 准备数据
        Object[] obj1s={"小花1",20,4};
        Object[] obj2s={"小花2",21,5};
        Object[] obj3s={"小花3",22,6};
        Object[] obj4s={"小花4",23,7};

        // 添加到List集合
        List<Object[]> list=new ArrayList<>();
        list.add(obj1s);
        list.add(obj2s);
        list.add(obj3s);
        list.add(obj4s);

        // 执行sql
        int[] count = jdbcTemplate.batchUpdate(sql, list);
        System.out.println(Arrays.toString(count));
    }

    @Test
    public void testBatchDelete(){
        // 删除多条语句
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        String sql="delete from t_user where id=?";

        // 准备数据
        Object[] obj1s={4};
        Object[] obj2s={5};
        Object[] obj3s={6};
        Object[] obj4s={7};

        // 添加到List集合
        List<Object[]> list=new ArrayList<>();
        list.add(obj1s);
        list.add(obj2s);
        list.add(obj3s);
        list.add(obj4s);

        // 执行sql
        int[] count = jdbcTemplate.batchUpdate(sql, list);
        System.out.println(Arrays.toString(count));
    }

    @Test
    public void testCallBack(){
        // 如果你想使用回调函数的话，可以使用callBack回调函数
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        JdbcTemplate jdbcTemplate = applicationContext.getBean("jdbcTemplate", JdbcTemplate.class);
        // 准备sql语句
        String sql="select id,real_name,age from t_user where id=?";
        // 注册回调函数，当execute方法执行的时候，回调函数中的doInPreparedStatement()会被调用
        User user = jdbcTemplate.execute(sql, new PreparedStatementCallback<User>() {
            @Override
            public User doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                User user = null;
                ps.setInt(1, 2);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String real_name = rs.getString("real_name");
                    int age = rs.getInt("age");
                    user = new User(id, real_name, age);
                }
                return user;
            }
        });
        System.out.println(user);
    }
}

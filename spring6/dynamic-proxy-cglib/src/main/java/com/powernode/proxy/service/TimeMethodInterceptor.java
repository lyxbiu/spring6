package com.powernode.proxy.service;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class TimeMethodInterceptor implements MethodInterceptor {
    /**
     * @param proxy 代理对象
     * @param method 目标方法
     * @param objects 目标方法调用时的实参
     * @param methodProxy 代理方法
     * @return
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        // 前后增强
        long begin=System.currentTimeMillis();

        // 调用代理对象上代理方法的父类方法
        Object retValue = methodProxy.invokeSuper(proxy, objects);

        // 后面增强
        long end=System.currentTimeMillis();
        System.out.println("耗时："+(end-begin)+"毫秒");

        return retValue;
    }
}

package com.powernode.spring6.bean;

import org.springframework.stereotype.Service;

@Service
public class Order {
}

/**
 * 以上这个注解@Service就相当于以下配置
 * <bean id="order" class="com.powernode.spring6.bean.Order"/>
 */

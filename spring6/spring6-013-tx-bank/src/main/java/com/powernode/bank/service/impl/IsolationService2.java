package com.powernode.bank.service.impl;

import com.powernode.bank.dao.AccountDao;
import com.powernode.bank.pojo.Account;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service("i2")
public class IsolationService2 {
    @Resource(name = "accountDao")
    private AccountDao accountDao;

    // 负责inset
    // @Transactional(timeout = 10,readOnly = true) // 设置事务超时时间为10s
    // @Transactional(rollbackFor = RuntimeException.class) // 只要发生RuntimeException及这个异常的子类异常，都回滚
    @Transactional(noRollbackFor = NullPointerException.class) // 当异常是空指针异常时不回滚
    public void save(Account act) throws IOException {
        // 睡眠一会
        /*try {
            Thread.sleep(1000*12);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/
        accountDao.insert(act);

        // 模拟异常
        if (1==1){
            // throw new IOException();
            throw new RuntimeException();
            // throw new NullPointerException();
        }

        // 睡眠一会
        /*try {
            Thread.sleep(1000*12);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/
    }
}

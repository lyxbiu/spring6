package com.powernode.client;

import com.powernode.annotation.Component;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ComponentScan {
    public static void main(String[] args) {
        Map<String,Object> beanMap=new HashMap<>();
        // 目前只知道一个包的名字，扫描这个包下的所有类，当这个类上有@Component注解的时候，实例化该对象，然后方到Map集合中
        String packageName="com.powernode.bean";
        // 开始扫描程序
        // . 这个正则表达式代表任意字符
        // 在正则表达中怎么表示一个普通的"."字符呢？使用 \. 正则表达式表示一个普通的.
        String packagePath = packageName.replaceAll("\\.", "/");
        // System.out.println(packagePath);
        // com是在类的根路径下的一个目录
        URL url = ClassLoader.getSystemClassLoader().getResource(packagePath);
        // 拿到绝对路径
        String path = url.getPath();
        // System.out.println(path);
        // 获取一个绝对路径下的所有子文件
        File file=new File(path);
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(f -> {
            try {
                // System.out.println(f.getName().split("\\.")[0]);
                String className=packageName+"."+f.getName().split("\\.")[0];
                // System.out.println(className);
                // 通过反射机制解析注解
                Class<?> clazz = Class.forName(className);
                // 判断类上是否有这个注解
                if (clazz.isAnnotationPresent(Component.class)) {
                    // 获取注解
                    Component annotation = clazz.getAnnotation(Component.class);
                    String id = annotation.value();
                    // 有这个注解的都要创建对象
                    Object obj = clazz.newInstance();
                    beanMap.put(id,obj);
                }
            }catch (Exception e){

            }
        });
        System.out.println(beanMap);
    }
}

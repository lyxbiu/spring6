package com.powernode.spring6.bean;

import org.springframework.stereotype.Controller;

@Controller("vipBean")
public class Vip {
}

/**
 * 以上这个注解@Controller就相当于以下配置
 * <bean id="vipBean" class="com.powernode.spring6.bean.Vip"/>
 */

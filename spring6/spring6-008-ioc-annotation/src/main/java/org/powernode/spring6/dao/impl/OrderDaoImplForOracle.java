package org.powernode.spring6.dao.impl;

import org.powernode.spring6.dao.OrderDao;
import org.springframework.stereotype.Repository;

// @Repository("orderDaoImplForOracle")
public class OrderDaoImplForOracle implements OrderDao {
    @Override
    public void insert() {
        System.out.println("Oracle数据库正在处理订单信息！！！");
    }
}

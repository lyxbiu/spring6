package com.powernode.factory.method;

// 抽象工厂角色
public abstract class WeaponFactory {
    // 这个方法不是静态的，是实例化的
    public abstract Weapon get();
}
